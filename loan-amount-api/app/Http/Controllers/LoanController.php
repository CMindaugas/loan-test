<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;

class LoanController extends Controller
{

    /**
     * Count score by personal code, amount and period
     *
     * @param $personalCode
     * @param $loanAmount
     * @param $loanPeriod
     * @return void
     */
    public function countLoan($personalCode, $loanAmount = null, $loanPeriod = null)
    {
        $path = base_path('config/persons.json');
        try {
            $data = json_decode(file_get_contents($path), true);

            foreach ($data as $person) {
                if (isset($person[$personalCode])) {
                    if ($loanAmount && $loanPeriod) {
                        $creditScore = ($person[$personalCode]['creditModifier'] / $loanAmount) * $loanPeriod;
                        $person[$personalCode]['creditScore'] = $creditScore;
                        $maxLoanAmount = $person[$personalCode]['creditModifier'] * $loanPeriod;
                        $person[$personalCode]['maxLoanAmount'] = $maxLoanAmount;
                        $minLoanPeriod = $loanAmount / $person[$personalCode]['creditModifier'];
                        $person[$personalCode]['minLoanPeriod'] = $minLoanPeriod;
                    }

                    return response()->json($person[$personalCode], 200);
                }
                return response()->json([
                    'error' => [
                        'message' => 'Personal code not found'
                    ]
                ], 404);
            }
        } catch (Exception $e) {

            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }
}
