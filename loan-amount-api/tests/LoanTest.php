<?php

class LoanTest extends TestCase
{
    /**
     * @return void
     */
    public function testExistingCode()
    {
        $this->get('/api/getCreditScore/49002010965', [])
            ->seeStatusCode(200)
            ->seeJson([
                'hasDebt' => true,
                'creditModifier' => null
            ]);
    }

    /**
     * @return void
     */
    public function testExistingCodeAmountAndPeriod()
    {
        $this->get('/api/getCreditScore/49002010976/3000/24', [])
            ->seeStatusCode(200)
            ->seeJson([
                'hasDebt' => false,
                'creditModifier' => 100,
                'creditScore' => 0.8,
                'maxLoanAmount' => 2400,
                'minLoanPeriod' => 30
            ]);
    }

    /**
     * @return void
     */
    public function testNotExistingCode()
    {
        $this->get('api/getCreditScore/49002010974')
            ->seeStatusCode(404)
            ->seeJson([
                'error' => [
                    'message' => 'Personal code not found'
                ]
            ]);
    }

    /**
     * @return void
     */
    public function testWithoutCode()
    {
        $this->get('/api/getCreditScore', [])
            ->seeStatusCode(404)
            ->seeJson(['message' => 'Page not found']);
    }
}
