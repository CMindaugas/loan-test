# Loan project #

Loan project based on Lumen and Nuxt+Vuetify. 

### Getting started ###
First, clone the repo:
```
$ git clone https://CMindaugas@bitbucket.org/CMindaugas/loan-test.git
```
### Start API ###
Navigate to loan-amount-api directory an run `docker-compose up` or `docker-compose up -d` command. Example:

```
$ cd loan-amount-api
$ docker-compose up
```

API will run on 8000 port, so make sure this port is available on your computer.

### API Routes ###
| HTTP Method	|  Path | Description  |
| ----- | ----- | ----- | 
| GET      | /api/getCreditScore/`{personal_code}` | Will show if person has dept |
| GET      | /api/getCreditScore/`{personal_code}`/`{loan_amount}`/`{loan_period}` | Get all person information |

### Example data ###
| Personal code	|  Description  |
| ----- | ----- | 
| 49002010965 | has debt |
| 49002010976 | segment 1 (credit_modifier = 100) |
| 49002010987 | segment 2 (credit_modifier = 300) |
| 49002010998 | segment 3 (credit_modifier = 1000) |

### Start UI ###
Navigate to loan-amount-ui direcory and run `docker-compose up` or `docker-compose up -d` command. Example:

```
$ cd loan-amount-ui
$ docker-compose up
```

Make sure 80 port is available on your computer, because UI will run on it. You can change port in `docker-compose.yml` file ~9 line:

```
1. version: "3.5"
2. services:
3.   front:
4.     image: node:16-slim
5.     working_dir: "/var/www/app"
6.     environment:
7.       - HOST=0.0.0.0
8.     ports:
9.       - "80:3000"
10.     volumes:
11.       - .:/var/www/app
12.     command: bash -c "npm install && npm run dev"
```
### Start UI (without docker-compose) ###

There is also possible to run UI without docker, just make sure you have installed latest LTS node version. 

First you need to install node modules:

```
$ cd loan-amount-ui
$ npm run install
```

Then run UI in dev mode:
```
$ npm run dev
```

Application will run on 3000 port, if it's available.


### Run API tests ###
```
$ cd loan-amount-api
$ docker-compose exec lumen php ./vendor/bin/phpunit
```
